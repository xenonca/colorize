# Colorize

## Description
Let's you colorize messages using HEX color codes (for testing purposes).

## Usage
Use `/c <HEX-code> <message>` to return a message colorized in your specified color.  
Use `/ca <HEX-code> <message>` to send a message colorized in your specified color to the public chat.  

## Dependencies
--------------
none

## License
---------
MIT by xenonca  

[![ContentDB](https://content.minetest.net/packages/_Xenon/colorize/shields/title/)](https://content.minetest.net/packages/_Xenon/colorize/)
