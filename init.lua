minetest.register_privilege("colorize", {
	description = "Colorize messages with HEX codes",
	give_to_singleplayer = false
})

minetest.register_chatcommand("c", {
	params = "<color> <msg>",
	privs = {colorize = true},
	description = "Return colorized message | Example: /c FF9900 Hello",
	func = function(name, param)
		if not param or param:trim() == "" then
			return false, "Please enter a message!"
		end

		local args = param:split(" ")
		if #args < 2 then
			return true, "Please enter a HEX code!"
		end

		if args[1]:len() ~= 6 or not args[1]:match("[%dabcdefABCDEF]") then
			return false, "Invalid HEX code. (Do not inclue '#' in your message)"
		end

		local color = tostring(args[1])
		table.remove(args, 1)
		local msg = table.concat(args, " ")
		minetest.chat_send_player(name, minetest.colorize("#" .. color, msg))
	end
})

minetest.register_chatcommand("ca", {
	params = "<color> <msg>",
	privs = {colorize = true},
	description = "Send colorized message | Example: /ca FF9900 Hello",
	func = function(name, param)
		if not param or param:trim() == "" then
			return false, "Please enter a message!"
		end

		local args = param:split(" ")
		if #args < 2 then
			return true, "Please enter a HEX code!"
		end

		if args[1]:len() ~= 6 or not args[1]:match("[%dabcdefABCDEF]") then
			return false, "Invalid HEX code. (Do not inclue '#' in your message)"
		end

		local color = tostring(args[1])
		table.remove(args, 1)
		local msg = table.concat(args, " ")
		minetest.chat_send_all(minetest.colorize("#" .. color, msg))
	end
})